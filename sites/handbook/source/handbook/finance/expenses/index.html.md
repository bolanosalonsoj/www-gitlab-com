---
layout: handbook-page-toc
title: Expenses
---
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

<div class="panel panel-gitlab-orange">
**This is a Controlled Document**
{: .panel-heading}
<div class="panel-body">

Inline with GitLab's regulatory obligations, changes to [controlled documents](/handbook/security/controlled-document-procedure.html) must be approved or merged by the AP team, as our Expense policy is a formal policy. All contributions are welcome and encouraged.

</div>
</div>

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Introduction

Welcome to the Expenses page! You should be able to find answers to most of your questions here. If you can't find what you are looking for, then:  

- **Email**: `expenses@gitlab.com`
- [**Navan Expense Guide**](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/navan-expense-guide/)

GitLab utilizes Navan as our team member expense reimbursement tool. All team members will have access to Navan as part of the Onboarding process. If you didn't receive an email from Navan for your access, please contact the AP team through one of the channels listed above.
**Note**: Ensure to opt-out from news by logging in and navigating to `Profile > Notifications > Product updates and communication` and un-checking the box.

The procedure by which reimbursable expenses are processed varies and is dependent on contributor legal status (e.g. independent contractor, employee) and [subsidiary assignment](/handbook/tax/#tax-procedure-for-maintenance-of-gitlabs-corporate-structure). Check with Payroll if you are unsure about either of these.

Team members should also consider the terms and conditions of their respective contractor agreements when submitting invoices to the company.

Team members are reimbursed on different schedules depending on their location. Please see the reimbursement process below to find the listing of reimbursement dates based on your location or employment status.

## <i class="far fa-flag" id="biz-tech-icons"></i> General Guidelines

* Itemized receipts are required for all expenses.  Receipts must contain vendor name, date of purchase, listing of what was purchased, and contains a full breakout of the subtotal and tax amounts.
* Items over 90 days from purchase date will not be reimbursed.
* The maximum monthly limit to be reimbursed is $5K USD (or local currency equivalent).
* Expenses that span longer than a month must be submitted on a monthly basis (i.e. although you paid for a year upfront for the internet, you must divide the total by 12 months and submit the cost monthly).
* Gift cards are not accepted as a form of payment for business expenses.
* Expense report items must clearly state if the spend includes amounts for clients / non-team members. Tax requirements in certain countries require us to account for client spending differently.
* All internet/telephone reimbursements must have a detailed receipt attached which shows the amount, service, and service date.
   - Please note we do not reimburse late fees, initial set up, or equipment costs, including ipads, mobile phone handsets, airpods, routers.
   - We do not reimburse for additional services on your telecoms provider bill, i.e. Television package, additional sim cards, family packs.
   - If an itemized bill is not available , costs will only be reimbursed for the element relating to Internet Access , i.e. if you pay for tv, internet & phone - the invoice should be divided by 3 /# of services and we will pay this value only.
* Anything submitted outside of these guidelines will be flagged as Out of Policy.
* All work-related expenses (internet, co-working space, other monthly subscription, etc.) are not reimbursable while on Leave of any kind.

### Out of Policy Transactions

* The AP Team will administer the policy as per the Expense Handbook.
* Note that expenses flagged as “Out of Policy” does not necessarily mean it is “out of policy”- it can also mean that certain expenses require additional review due to requiring certain attachments (ie an approved issue) and approvals.
* Upon review, if more clarification is needed for a certain expense, the AP team will contact the team member in the Navan tool to ask for justification of the transaction before approving or rejecting the expense.
* The AP team will reach out to the team member’s manager if additional approval is needed.
* If the claim is over the advised guidance, the claim will be rejected with a note for the team member.  The team member should be able to rectify the claim and resubmit.
* Any claim made which does not fall into the policy is not reimbursable.
* There will be occasions where if you have claimed an item and it needs to be reimbursed back to Gitlab, you will need to follow the reimbursement process in Navan for submission of the funds.

### Manager Expectations

* Managers are expected to review their team’s expenses in their entirety every month, and must flag any concerns immediately to the Accounts Payable team.
* Managers will be held accountable for any unreported abuse of their team members.
* Managers must be aware of the policy guidance and advise team members as such when requests for purchase are made.
* AP will flag Managers if further information is required for their direct reports.
* Please see the attached [Manager Policy](https://docs.google.com/document/d/1HQF-_fDIkjsmq-ExsFQwwTW-x8rocGKSDRZVdMYQudA/edit) for more details.

## Expense Policy Outline

Please note that although the policy says “$0.00” in some expense categories of the policy in Navan,  it does not mean that it is not reimbursable. It just means that all expenses submitted under that category will need to be reviewed by the AP team.

All amounts listed below are in USD. Your Navan Profile will default your base currency as per your geographical location.  Please refer to “How Exchange Rates are Calculated” section below.

| Expense Category | Policy | GL Code NON COGS | GL Code COGS | Navan Policy Category | Limit |
| ------ | ------ | ------ | ------ | ------ | ------ |
| AirFare | Refer to the [Travel Handbook Page](https://about.gitlab.com/handbook/travel/). | 6003 - Airfare | 5003 - Airfare COGS | Airfare & fees | - To be booked in Navan.<br>- When booked outside Navan - will flag approval required by AP Admin. |
| Books (Audio /E-Books included) | All books are reimbursable if used to optimize your job position. | 6021 - Professional dues, membership fees | 5021 - Professional dues, membership fees COGS | Books, dues & subscriptions | Approval required by AP Admin for Navan for all purchases to ensure reasonableness of expense. |
| Business Cards | Business cards are ordered from Moo as per the instructions provided by the People Experience team. Urgent Business cards needed for the day of start can be requested by emailing people-exp@gitlab.com. As a last resort, Moo does offer 3 to 4 Day Express service. | 6040 - Office Supplies | 5040 - Office Supplies COGS | Office Supplies | To be ordered via Moo, attach receipt from Moo. |
| Car Rental | Refer to the [Travel Handbook Page](https://about.gitlab.com/handbook/travel/). | 6063 - Taxis, Car Service, Public Transportation | 5063 - Taxis, Car Service, Public Transportation COGS | Rental cars | - To be booked in Navan.<br>- When booked outside Navan - will flag approval required by AP Admin. |
| Co-Working Space | Coworking space is reimbursable. Must be pre-approved by Manager, and Finance (via email to expenses@gitlab.com ) before expensing. No contracts are allowed to be under the GitLab name and it must be under team members. Gitlab will not cover security deposits or advance payments for co-working space. The co-working space must be submitted monthly in the month it pertains to. | 6076 - Co Working Space | 5076 - Co Working Space | Other | - $700.00 Limit per month.<br>- Full Receipts to the provided.<br>- Copy of approved email must be submitted with first month submission.<br>- If co-working space is expensed in the month then you are only able to expense the internet reimbursement  for the days that the co-working space is not paid for. i.e. if you have a contract for co-working space for the month, then you are unable to claim Internet as an expense in that month. This is also applicable on a pro-rata basis. |
| Conferences | All costs associated with attending a conference to represent GitLab are reimbursable and an approved [completed G&D issue](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) will need to be noted in the expense report upon submission. Select classification or tag “Growth and Development” in Navan when you create expenses. | 6065 - Training & Development | 5065 - Training & Development COGS | Conference attendance | - Needs to have approved G&D Development issue.<br>- AP Admin needs to review expense for approved issue. |
| Equipment | Please refer to our [Home Office Equipment Handbook Page](https://about.gitlab.com/handbook/finance/procurement/office-equipment-supplies/). | 6016 - Computer & Office Equipment - Expensed | 5016 - Computer & Office Equipment - Expensed COGS | Work from home | - For Year 1 ($1,500.00) Stipend through Virtual Card.<br><br>- Year 2 onwards ($500.00) - Expensed via Navan. |
| Get Together/Visiting Grant | Please refer to our [Incentives at Gitlab Handbook Page](https://about.gitlab.com/handbook/incentives/#get-together-grant) | 6039 - Get Togethers | 5039 - Get Together COGS | Team events & meals | Set by management when available. |
| Gifts for Significant Life Events and Team Member Gifts | Please refer to our [Celebrations and Significant Life Events Handbook Page](https://about.gitlab.com/handbook/people-group/celebrations/). | 6028 - Gifts internal | 5028 - Gifts Internal COGS | Gifts - internal | - Gift Cards/Cash are not Allowed; Qualified gifts are capped at $125 per transaction.<br>- Approval required by AP Admin for Navan for all purchases. |
| Hotel | Refer to the [Travel Handbook Page](https://about.gitlab.com/handbook/travel/). | 6027 - Hotels & Lodging | 5027 - Hotels & Lodging COGS | Lodging | - To be booked in Navan.<br>- When booked outside Navan - approval required by AP Admin. |
| Internet | All monthly internet bills are reimbursable up to $80.00, but GitLab will not reimburse for any initial setup/change fees. Please expense internet costs monthly, reflecting current charges only (even if you pay by a different cadence).<br><br>**For team members in the Netherlands:** if you have not already completed the form as part of onboarding with our Netherlands payroll provider, fill in and sign the [Regeling Internet Thuis](https://docs.google.com/a/gitlab.com/document/d/1J70geARMCjRt_SfxIY6spdfpTbv_1v_KDeJtGRQ6JmM/edit#heading=h.5x5ssjstqpkq) form and send it to the People Experience team at people-exp@gitlab.com. The People Experience team will then send it to the payroll provider in the Netherlands via email. The details of the payroll provider can be found in the PeopleOps vault in 1Password under "Payroll Contacts".<br><br>**Important:** This is the process to be followed for compliance reasons for Internet expenses for the Netherlands. Please do not expense your Internet costs via Navan - these will be rejected.<br><br>If your address changes or the amount changes, please send a new form to the People Experience team, along with the invoice/letter for processing.<br><br>This is a taxable expense for GitLab Ltd. team members assuming that the internet connection is used partially for personal use as well as business use. | 6031 - Internet | 5031 - Internet COGS | Internet access | - Up to $80.00 per month.<br>- Itemized Receipt required.<br>- If an itemized bill is not available, costs will only be reimbursed for the element relating to Internet Access, i.e. if you pay for tv, internet and phone, the invoice should be divided by 3/# of services and we will pay this value only.<br>- No monthly hardware costs for internet service is reclaimable.<br>- If co-working space is also expensed in the month then you are only able to expense the internet reimbursement for the days that the co-working space is not paid for. i.e. if you have a contract for co-working space for the month, then you are unable to claim Internet as an expense in that month. This is also applicable on a pro-rata basis. |
| Laptops, insurance and repairs | All laptops are purchased through GitLab unless an employee is in a region where GitLab cannot deliver a laptop. Team member laptops can be refreshed after three years of employment. If a team member needs a replacement laptop due to damage, they must reach out to IT via an issue before purchasing a new one. See [Laptop handbook page](/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptops) for your reference.<br><br>**Repairs to company issued equipment**<br><br>If you need to replace a battery or something small that does not affect the productivity or speed of the device, please go ahead and get that small item replaced and expensed. The category to use in Expensify is 'Laptop'.<br>Please get approval from your Manager if your equipment appears to be damaged, defective, or in need of repair. Business Operations can advise on next steps to ensure you have the proper equipment to work.<br>For loaner laptops: Do not hesitate when expensing a loaner laptop while your primary laptop is being repaired. Use your best judgment identifying a local vendor. Please check out our [Laptop Repair](/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptop-repair) page for more info. | 1403 - Computer and Office Equipment (Laptop Purchase)<br><br>6016 - Computer and Office Equipment - Expense (Laptop Repair) | 1403 - Computer and Office Equipment (Laptop Purchase )<br><br>5016 - Computer and Office Equipment - Expense (Laptop Repair) | Tools and Materials | - Needs to have approved issue from IT team.<br>- AP Admin needs to review expense for approved issue. |
| Marketing- related expenses | Small purchases for marketing events which are under $2,500 USD and which cannot be paid through the Procurement Process (Zip) are reimbursable. | Multiple- will be required to choose GL account | Multiple- will be required to choose GL account | Other | - Up to $2500 can be expensed via Navan.<br><br>- >$2500 needs to go through procurement process in [Zip](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/zip-guide/). |
| Meals with Clients and Partners | These are reimbursable, however team members should use their discretion while claiming meals with clients and partners. | 6013 - Business Meals & Entertainment | 5013 - Business Meals & Entertainment COGS | Entertaining clients | $1,500.00 per transaction. |
| Meals: Company Provided while Traveling | To be used for team activities and food/beverages associated with travel. Refer to our [Travel Handbook Page](https://about.gitlab.com/handbook/travel/) | 6041 - Meals - Company Provided | 5041 - Meals - Company Provided COGS | Traveling: meals for myself | $100 per day. |
| Mileage | Mileage is reimbursed according to local law: US rate per mile, rate per km in the Netherlands, or rate in Belgium. Refer to "How Mileage Rates are Based" section below. | 6046 - Parking, Gas, Tolls, Mileage | 5046 - Parking, Gas, Tolls, Mileage COGS | Distance Driven | - Based on Mileage driven, reimbursed depending on country/local law.<br>- Should Show Start and End point of trip in Navan. |
| Office Supplies & consumables | To maintain your home office equipment, items like compressed air, pens, etc may be required from time to time. | 6040 - Office Supplies | 5040 - Office Supplies COGS | Office Supplies | $15.00 per month. |
| Phone Service | Cell phone service is reimbursable up to $50.00 monthly threshold for employees whose phones are vital to their position. Actual cell phone, phone accessories and family plans are not reimbursable. | 6064 - Telephone | 5064 - Telephone COGS | Cell phone | - $50.00 per month.<br>- Itemized Receipt required.<br>- If itemized bill, is not available, then the costs will only be reimbursed for the element relating to Internet Access, i.e. if you pay for tv, internet and phone, the invoice should be divided by 3/# of services and we will pay this value only.<br>- No monthly hardware costs for internet service is reclaimable.<br>- Tablets. Watches, Phone Device are not reimbursable. |
| Postage/Shipping/Customs Fees | Postage/shipping/custom charges related to the sending and or receiving of work documentation or special packages can be reimbursed. | 6049 - Postage & Shipping | 5049 - Postage & Shipping COGS | Shipping & postage | $100.00 per transaction. |
| Professional dues, membership fees | All costs associated with professional dues and membership fees are reimbursable and an approved [completed G&D issue](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) will need to be noted in the expense report upon submission. Select classification or tag “Growth and Development” in Navan when you create expenses. | 6021 - Professional dues, membership fees | 5021 - Professional dues, membership fees COGS | Books, dues & subscriptions | - Needs to have approved G&D Development issue.<br>- AP Admin needs to review expense for approved issue. |
| Software | All software subscriptions are not reimbursable. Team members that would like to purchase software will need to go through the Procurement process. Please refer to the [Individual Use Software Handbook Page](https://about.gitlab.com/handbook/finance/procurement/personal-use-software/) | N/A | N/A | N/A | - Not Reimbursable.<br>- Please follow Guidance from Handbook. |
| Team Building | To be used for team activities and food/beverages associated with special team events. Amount allowed is dependent on their department budget for the year. | 6071 - Team Building | 5071 - Team Building COG | Team events & meals | $50 a quarter. |
| Training/Tuition | All costs associated with training or tuition are reimbursable (up to $10K) and an approved [completed G&D issue](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/) will need to be noted in the expense report upon submission. Select classification or tag “Growth and Development” in Navan when you create expenses. | 6065 - Training & Development COGS | 5065 - Training & Development COGS | Other | - Needs to have approved G&D Development issue.<br>- AP Admin needs to review expense for approved issue. |
| Transportation/Parking | Parking, Tolls, Taxi, Uber pertaining to travel to/from GitLab events, clients, or conferences is acceptable | 6046 - Parking, Gas, Tolls, Mileage | 5046 - Parking, Gas, Tolls, Mileage COGS | Public transport, tolls & parking | $300.00 per transaction. |
| Travel (Other) | Includes internet while traveling. Please refer to our [Travel Handbook Page](https://about.gitlab.com/handbook/travel/) | N/A | N/A | N/A | N/A |
| VPN Subscription | Please read [Why We Don't Have A Corporate VPN](https://about.gitlab.com/handbook/security/#why-we-dont-have-a-corporate-vpn), and check out our [Personal VPN page](https://about.gitlab.com/handbook/tools-and-tips/personal-vpn/) regarding usage at GitLab. | 6060 - Software Subscriptions | 5060 - Software Subscriptions | Other | - To be claimed per month.<br>- Itemized Receipt required. |
| Wire fees (Equity compensation) | Team members **outside of the USA** where there is no option to disburse equity funds from E*TRADE without incurring wire fees may expense those fees. | 6008 - Bank Fees | 5008 - Bank Fees COGS | Other | Will flag approval required by AP Admin. |

### Missing a Receipt?

If a team member is missing a receipt, a Missing Receipt Affidavit will need to be completed and attached to the expense when submitted.
Missing Receipt Affidavit located [here](https://docs.google.com/spreadsheets/d/179q0Wos-CemLCe1uxgMpZ80JukSC4KYx8JICFgHzN0I/edit?usp=sharing) (please ensure to make a copy).
When attaching the receipt affidavit, you should attach the document as a PDF.
To save the file as a PDF - open the document, click on FILE>DOWNLOAD>PDF.

### How Exchange Rates are Calculated

The handbook [uses USD when describing budgets](https://about.gitlab.com/handbook/handbook-usage/#fine-points). When budgeting and submitting expenses in other currencies, team members should use live currency conversion rates (exchange rates). These can be looked up online using a site such as [OANDA](https://www.oanda.com/currency-converter/en/).

**Example:** a team event has a meal budget of USD25 per person. A team member notes that this is $50 in local currency, at current exchange rates. The team member can purchase a meal in their local currency up to $50, and expense it in their local currency.

**Example:** when visiting other countries - you should convert the currency to your local currency when submitting the expense. Currency rate should be done using rates on the day the expense occurred (these can be looked up online using a site such as [OANDA](https://www.oanda.com/currency-converter/en/)). i.e. if you spent EUR50 whilst visiting Germany but you live in the UK, you should convert the EUR50 to GBP43.66 (your local Currency) and submit the expense with the valid receipt.

Note: live exchange rates do not apply to compensation, which [uses fixed exchange rates](https://about.gitlab.com/handbook/total-rewards/compensation/#exchange-rates).

### How Mileage Rates are Based

The mileage rate is based on your legal entity’s mileage rates for the current year.  These will be updated yearly for tax compliance.  However, for the following entities (due to limited visibility online), the default rate is set at $.50/mile… so please update the rate on your mileage if needed to match your local compliance laws:  Gitlab IT BV, Gitlab Korea and Gitlab Japan.

## Other Useful Information

### Team Building Budget

In FY23, each eGroup member has been allocated $50 per team member per quarter for FY23-Q1 to FY23-Q3 for team building events. For FY23-Q3 each eGroup member may determine if they wish to use their team building allocation at Contribute or for a different team building event. There is no additional team building budget for Contribute above the $50 per team member. In FY23-Q4 there is an additional budget of $100 per team member. 

### <i class="fas fa-bullseye" id="biz-tech-icons"></i> Year-end Holiday Party Budget

GitLab has allocated [$100 USD](https://www1.oanda.com/currency/converter/) per GitLab team member for a holiday event in Q4 (until end of January) for the different year-end holiday celebrations. 

Guidance on the year-end holiday budget:
- Each Egroup member will coordinate for their respective organizations or appoint a designee for the organization
- The budget can be used for in person or virtual events
- The budget is [$100 USD](https://www1.oanda.com/currency/converter/) per GitLab team member and funds cannot be pooled or re allocated if there is under spend
- The money cannot be used for charitable donations or gifts
- If you are hosting an event and asked to attend another event, the budget is for all events and not per event
- The budget can be spent anytime in Q4, please make sure you promptly submit your expense report prior to the end of Q4
- In Expensify, the team member must code any related expense to the Category "Company Functions" and to the Classification "Holiday Celebration"

If you have any questions, please reach out to your manager for guidance.

### Get Together/Virtual Meal with GitLab Team member(s)

Please refer to our [Incentives at Gitlab Handbook Page](/handbook/incentives/#get-together-grant)

### Domain Name Registration and Maintenance Policy

#### Scope and Purpose
This policy applies to all GitLab team members registering, or maintaining a registration for, GitLab-related Domain Names, including GitLab-related Domain Names registered or used for sandbox or testing purposes.

Domain names are key assets in GitLab's intellectual property portfolio. Centralizing the registration and maintainance of domain names under the Infrastructure Shared Services group using GitLab's approved domain name registrar helps us track and protect these valuable assets.

"**GitLab-related Domain Names**" when used in this policy means any domain name:
   - registered or used for any purpose related to a team member's role at GitLab;
   - containing the GitLab trademark (GITLAB) or one of its derivatives (like, amongst other things, git, glab, gtlb, gl); and/or
   - containing any GitLab key messaging term (like, amongst other things, devops, devops platform, all remote).
   
#### Registration of new GitLab-related Domain Names
- All GitLab-related Domain Names must be registered using the process outlined in the [dns-domain-purchase-request issue template](https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/issue-tracker/-/issues/new?issuable_template=dns_domain_purchase_request) maintained by IT Operations.
- Expense reports submitted by team members for expenses incurred on or after `2022-03-01` in registering new GitLab-related Domain Names will be rejected.

#### Transfer of existing GitLab-related Domain Names
- All GitLab-related Domain Names currently registered in the name of team members, or registered with unapproved registrars, should be transferred to GitLab using the process outlined in the [dns-domain-transfer-request issue template](https://gitlab.com/gitlab-com/business-technology/engineering/infrastructure/issue-tracker/-/issues/new?issuable_template=dns_domain_transfer_request) as soon as possible.
- Expense reports submitted by team members for expenses incurred on or after `2022-08-01` in maintaining registrations of GitLab-related Domain Names in the name of team members, or with unapproved registrars, will be rejected.

### Transport/Delivery of Second Hand and Free Procurements
Feel free to check local second-hand/free markets when looking for equipment, especially furniture such as desks and chairs. GitLab will reimburse the cost of any transport and delivery services you need to procure the item.

If you are able to find hidden gems in these markets, please go ahead and make the purchase. In order to expense these types of purchases, we ask that you take a screenshot or photo of the sale listing and any fund transfers or conversations between yourself and the seller on price and delivery costs. 

## Reimbursement Payout Timelines

### Legal entities:
* Team members must submit their expenses through Navan.
* Expenses for GitLab Inc, GitLab Federal, GitLab Ltd (UK), GitLab BV (Belgium and Netherlands), GitLab GmbH (Germany), GitLab PTY Ltd (Australia and New Zealand), GitLab GK (Japan) GitLab LTD (Ireland), GitLab Korea Limited and GitLab Singapore PTE. LTD. are reimbursed via Navan within 7 business days for the expenses within policy or after approval by the Accounts Payable team (for out of policy expenses).
* The team member's bank account must be set up in Navan in order for payment to complete.
* Expenses for GitLab Canada Corp must be submitted and approved by the Accounts Payable team (for out of policy expenses) before the 1st day of each payroll period.
* Expenses for GitLab France S.A.S and GitLab BV Finland must be submitted and approved by the Accounts Payable team (for out of policy expenses) on or before the 6th of each month to ensure it is included in the current month’s payroll.

### PEO (Professional Employer Organization/ Employer of Record and not a GitLab entity or Branch):

#### Global Upside, Remote.com and Papaya Global

* The list of Global Upside, Remote & Papaya countries can be found [here](/handbook/people-group/employment-solutions/#peo-professional-employer-organization-employer-of-record-and-not-a-gitlab-entity)
* Team members must submit their expenses through Navan. 
* All expenses must be submitted and approved by the Accounts Payable team (for out of policy expenses) on or before the 6th of each month to ensure it is included in the current month's payroll.

#### iiPay

* Team members must submit their expenses through Navan.
* All Individual contractors or C2C, with exception of Nigeria will be reimbursed by iiPay by the 22nd of each month. All expenses must be submitted and approved by the Accounts Payable team (for out of policy expenses) by the 6th of each month to be included in the current month payment. For contractors with C2C status, be sure to contact the Payroll team via email at nonuspayroll@gitlab.com and expenses@gitlab.com if you need to set up a separate bank for your expense reimbursement.

#### CXC Global

* The list of CXC countries can be found [here](/handbook/people-group/employment-solutions/).
* Team members must submit their expenses through Navan.
* All expenses must be submitted and approved by the Accounts Payable team (for out of policy expenses) on or before the 6th of each month to ensure it is included in the current month's payroll.
* GitLab Payroll Team will send the approved expense amount to CXC EMEA Payroll to include with the monthly salary.
* Team members must include the approved expense amount on their monthly invoice as well.

### Team Member Expense Temporary Advances

These instructions apply if a team member is unable to purchase required items, for whatever reason.

1. A request is sent to Payroll explaining the reason for the advance. uspayroll@gitlab.com or nonuspayroll@gitlab.com
2. [Expense reimbursement template](https://docs.google.com/spreadsheets/d/1D0kWlqol7jBjqn7yDY6uc7UCWSIcUmiF/edit?usp=sharing&ouid=108533621432009168804&rtpof=true&sd=true) is filled out and returned to Payroll.
    - Must include the correct entity, currency, VAT, valid receipts and banking details for payment.
3. Payroll reviews and approves/rejects. 
    - If approved, they forward the report to the VP, Corporate Controller, or Principal Accounting Officer to request approval for reimbursement. 
4. Once approved, payroll forwards the approval and reimbursement request to AP. 
    - The request must include valid banking details for the individual to receive payment.
5. AP will do their best to pay the reimbursement within 1 week, depending on the date submitted. 
    - Note that AP completed payments on Thursdays unless otherwise instructed for month and quarter end timelines.

### Spend reduction

When reducing spend, we'll not take the easy route of (temporarily) reducing discretionary spending.
Discretionary spending includes expenses like travel, conferences, gifts, bonuses, merit pay increases and Contributes.
By reducing in these areas we put ourselves at risk of [increasing voluntary turnover among the people we need most](https://steveblank.com/2009/12/21/the-elves-leave-middle-earth-%E2%80%93-soda%E2%80%99s-are-no-longer-free/).
Discretionary spending is always subject to questioning, we are frugal and all spending needs to contribute to our goals.
But we should not make cuts in reaction to the need to reduce spend; that would create a mediocre company with mediocre team members.
Instead, we should do the hard work of identifying positions and costs that are not contributing to our goals.
Even if this causes a bit more disruption in the short term, it will help us ensure we stay a great place to work for the people who are here.

### Expense Reimbursement for Terminated Team Members

If a team member whom you managed has left GitLab and comes to you with final expenses that are valid for reimbursement, please verify that they were not already processed in Expensify and then contact Accounts Payable. You must submit valid receipts and a completed copy of the [Expense Reimbursement template](https://docs.google.com/spreadsheets/d/1D0kWlqol7jBjqn7yDY6uc7UCWSIcUmiF/edit?usp=sharing&ouid=108533621432009168804&rtpof=true&sd=true) along with your approval. Please note that we may also ask the terminated team member to provide valid banking details in order to process the payment to them. 
AP will do their best to process and pay the reimbursement to the individual within 1 week.

### Don’t See an Expense on the List?

Then the expense is not reimbursable.  Please refer to our [Zip](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/zip-guide/) handbook page on the steps for creating a purchase order or requesting a virtual card.

If an item that needs to be purchased is not on the list, then an issue will need to be created and submitted for approval from Department VP and PAO/Finance approval. This needs to be done prior to purchasing the item or it will be rejected.

- - -

Return to the main [finance page](/handbook/finance/).
